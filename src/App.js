
import Menu from './components/menu';
import Home from './components/home';
import Video from './components/video';
import Account from './components/account';
import Auth from './components/auth';
import Main from './components/mainRouter';
import Notfound from './components/Notfound';
import View from './components/view';
import Welcome from './components/welcome';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom';
import React, { Component } from 'react'

export default class App extends Component {
  constructor(props)
  {
    super(props);
    this.changeAuth = this.changeAuth.bind(this);
    this.state={
      isAuth: false,
      data:[
        {
        id:1,
        title:"Welcome",
        description:"same value in your component, pass it explicitly as a prop with a different name",
        picture:"https://images.pexels.com/photos/670720/pexels-photo-670720.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
      },
      {
        id:2,
        title:"Welcome",
        description:"same value in your component, pass it explicitly as a prop with a different name",
        picture:"https://images.pexels.com/photos/670720/pexels-photo-670720.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
      },
      {
        id:3,
        title:"Welcome",
        description:"same value in your component, pass it explicitly as a prop with a different name",
        picture:"https://images.pexels.com/photos/670720/pexels-photo-670720.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
      },
      {
        id:4,
        title:"Welcome",
        description:"same value in your component, pass it explicitly as a prop with a different name",
        picture:"https://images.pexels.com/photos/670720/pexels-photo-670720.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
      },
      {
        id:5,
        title:"Welcome",
        description:"same value in your component, pass it explicitly as a prop with a different name",
        picture:"https://images.pexels.com/photos/670720/pexels-photo-670720.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
      },
      {
        id:6,
        title:"Welcome",
        description:"same value in your component, pass it explicitly as a prop with a different name",
        picture:"https://images.pexels.com/photos/670720/pexels-photo-670720.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
      },
    ]
    }
  }
  changeAuth = () => {
    this.setState({
      isAuth: true,
    });
  };

  render() {
    return (
      <div className="App">
      <Router>
        <Menu/>
        <Switch>
          <Route path='/' exact component={Main}></Route>
          <Route path='/Home' render={()=><Home data={this.state.data}/>}></Route>
          <Route path='/Video' component={Video}></Route>
          <Route path='/Account' component={Account}></Route>
          <Route path="/Auth" render={(props) => ( <Auth {...props} changeAuth={this.changeAuth} />)}/>
          <Route path='/view:id' render={(props)=><View data={this.props.data} {...props}></View>}></Route>
          <Route path="/Welcome" render={(props) => ( <Welcome {...props} isAuth={this.state.isAuth} />)}/>
          <Route path="*" component={Notfound}></Route>
        </Switch>
      </Router>
    </div>
    )
  }
}
