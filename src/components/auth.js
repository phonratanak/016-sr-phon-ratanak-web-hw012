
import { Container,Form} from 'react-bootstrap';
import React, { Component } from 'react';
import {Link} from 'react-router-dom';

export default class auth extends Component {
    render() {
        return (
            <Container>
                <Form>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" />
                    </Form.Group>
                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" />
                    </Form.Group>
                    <Link as={Link} to={'/Welcome'} >
                        <button className="btn btn-primary mt-4" onClick={this.props.changeAuth}>Submit</button>
                    </Link>
                </Form>
            </Container>
        )
    }
}

