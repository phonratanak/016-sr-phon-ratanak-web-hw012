import React, { Component } from 'react'
import { Container,Card,Button,Row,Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default class home extends Component {
    
    render() {
        let datas=this.props.data;
        let view=datas.map((d)=> 
        <Col md={4} style={{paddingTop:25}} key={d.id}>
            <Card>
                <Card.Img variant="top" src={d.picture} />
                <Card.Body>
                    <Card.Title>{d.title}</Card.Title>
                    <Card.Text>
                    {d.description}
                    </Card.Text>
                    <Button as={Link} to={`/view${d.id}`} variant="primary">Continue</Button>
                </Card.Body>
            </Card>
        </Col>
    );
        return (
            <Container>
            <Row >
                {view}
            </Row>
            
            </Container>
        )
    }
}

