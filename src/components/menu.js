import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Navbar,Nav,Container } from 'react-bootstrap';
import {Link} from 'react-router-dom';

const menu = () => {
    return (
    <Navbar bg="light" expand="lg">
    <Container style={{marginBottom:20}}>
      <Navbar.Brand as={Link} to="/">React Router</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link as={Link} to="/home">Home</Nav.Link>
          <Nav.Link as={Link} to="/Video">Video</Nav.Link>
          <Nav.Link as={Link} to="/Account">Account</Nav.Link>
          <Nav.Link as={Link} to="/Auth">Auth</Nav.Link>
        </Nav>
      </Navbar.Collapse>
      </Container>
    </Navbar>
    );
}
export default menu;
