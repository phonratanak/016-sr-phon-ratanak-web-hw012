import React, { Component } from 'react'
import {Link,BrowserRouter as Router,Route} from 'react-router-dom';
import { Container } from 'react-bootstrap';
const topics = [
    {
      name: 'Animation',
      id: 'Animation',
      description: 'Animation Category',
      resources: [
        {
          name: 'Action',
          id: 'Action',
        },
        {
            name: 'Romance',
            id: 'Romance',
        },
        {
            name: 'Comedy',
            id: 'Comedy',
          }
      ]
    },
    {
        name: 'Movie',
        id: 'Movie',
        description: 'Movie Category',
        resources: [
          {
            name: 'hollywood',
            id: 'Adventure',
          },
          {
              name: 'rambo',
              id: 'Comedy',
          },
          {
              name: 'china',
              id: 'China',
            }
            ,
          {
              name: 'documentation',
              id: 'Documentation',
            }
        ]
    }
  ]

  function Resource ({match}) {
    const ID = topics.find(({ id }) => id === match.params.topicId)
      .resources.find(({ id }) => id === match.params.subId)
    return (
      <div>
        <h3>{ID.id}</h3>
      </div>
    )
  }
  function Topic (props) {
    const topic = topics.find(({ id }) => id === props.match.params.topicId)
    return (
      <div>
        <h1>{ topic.description}</h1>
        <ul>
            {topic.resources.map((sub) =>
                <li key={sub.id}>        
                    <Link  to={`/Video/${props.match.params.topicId}/${sub.id}`} onClick={props.changes}>{sub.name}</Link>
                </li>        
            )}
        </ul>
        <Route path={'/Video/:topicId/:subId'} component={Resource} />
      </div>
    )
  }
export default class video extends Component {
  constructor(props)
  {
    super(props);
    this.state={
      msg:"Please Select A Topic.",
    }
  }
  changeMsg = () => {
    this.setState({
      msg: null,
    });
  };
    render() {
        let value=topics.map(({name,id})=>
            <li key={id}><Link to={`/Video/${id}`} onClick={()=>this.setState({msg:"Please select a topic."})}> {name} </Link></li>
        );
        return (
            <Container>
                <Router>
                    <div>
                        <ul>
                            {value}
                        </ul>
                    </div>
                    <Route  path={'/Video/:topicId'} render={(props) => ( <Topic {...props} changes={this.changeMsg} />)}></Route>
                    <h3>{this.state.msg}</h3>
                </Router>
            </Container>
        )
    }
}

