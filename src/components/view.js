import React from 'react'
import { Container } from 'react-bootstrap'

function view(props) {
    return (
        <Container>
            <h1>This is View [{props.match.params.id}]</h1>
        </Container>
    )
}
export default view
