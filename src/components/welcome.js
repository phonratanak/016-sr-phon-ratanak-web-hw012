import React from "react";
import {Container} from 'react-bootstrap';

function welcome({ isAuth, history }) {
  if (isAuth === false) {
    history.push("/Auth");
  }
  return (
    <Container>
        <h1>Welcome</h1>
    </Container>
  );
}
export default welcome;

