import React from 'react'

function Notfound() {
    return (
        <div>
            <h1>This page Not Found 404!</h1>
        </div>
    )
}

export default Notfound
