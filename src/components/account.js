
import React, { Component } from 'react'
import {Link,BrowserRouter as Router,Route} from 'react-router-dom';
import { Container } from 'react-bootstrap';
import queryString from 'query-string';
const list = [
    {
      name: 'Netfix',
      id: 'Netfix',
    },
    {
        name: 'Zillow Group',
        id: 'Zillow Group',
    },
    {
        name: 'Yahoo',
        id: 'Yahoo',
    },
    {
        name: 'Modus Create',
        id: 'Modus Create',
      }
]
function Show (props) {
    let pv=queryString.parse(props.location.search);
    return (
      <div>
      {pv.name === undefined ? (
        <h1>There is no name in the querystring</h1>
      ) : (
        <h1>The <span style={{color:'red'}}>name</span> in the query string is "{pv.name}"</h1>
      )}
      </div>
    )
  }
export default class account extends Component {
    render() {
        let value=list.map(({name,id})=>
            <li key={id}><Link to={`/Account/?name=${id}`} > {name} </Link></li>
        );
        return (
            <Container>
            <Router>
                <div>
                    <h1>Accounts</h1>
                    <ul>
                        {value}
                    </ul>
                </div>
                <Route  exact path={'/Account'} render={(props) => ( <Show {...props} />)}></Route>
            </Router>
        </Container>
        )
    }
}

